import React, { useContext } from 'react'
import { Context } from '../../../context/context'
import { User, UsersContainer, UsersTitle } from './users.style'

const Users: React.FC = () => {
    const context = useContext(Context)
    return (
        <UsersContainer>
            <UsersTitle>Users in chat:</UsersTitle>
            {
                context.userList.map((c: any) => {
                    return (
                        <User key={c}>
                            {c}
                        </User>
                    )
                })
            }
        </UsersContainer>
    )
}

export default Users