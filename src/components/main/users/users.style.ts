import styled from 'styled-components'

export const UsersContainer = styled.div`
    width: 20rem; 
    height: 100%;
    background: ${({ theme }) => theme.colors.grey};
    display: flex;
    flex-direction: column;
    padding: 2rem;


    @media (max-width: 960px){
        display: none;
    }
`

export const UsersTitle = styled.div`
    font-size: 2.4rem;
    color: ${({ theme }) => theme.colors.white};
`

export const User = styled.p`
    font-size: 1.8rem;
    color: ${({ theme }) => theme.colors.white};
    margin-top: 1rem;
`