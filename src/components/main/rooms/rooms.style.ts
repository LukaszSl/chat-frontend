import styled from 'styled-components'
import { darken } from 'polished'

export const RoomsContainer = styled.div`
    height: 100%;
    width: 35rem;
    padding: 4rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    background: ${({ theme }) => theme.colors.grey};

    @media (max-width: 1150px){
        width: 30rem;
    }

    @media (max-width: 750px){
        position: fixed;
        top: 0;
        left: 0;
        z-index: 1000;
        transition: all .2s;
        width: 25rem;
        padding: 4rem 2rem;
        transform: ${(props: { open: boolean }) => props.open ? 'translateX(0)' : 'translateX(-100%)'};
    }
`

export const ChatLogo = styled.img`
    height: 10rem;
    border-radius: 1rem;

    @media (max-width: 750px){
        height: 7rem;
    }
`

export const ChatName = styled.h1`
    font-size: 2.5rem;
    color: ${({ theme }) => theme.colors.white};
    text-align: center;
`

export const RoomsList = styled.div`
    margin-top: 5rem;
    width: 100%;
`

export const Room = styled.div`
    width: 100%;

    & span{
        text-align: left;
        width: 100%;
        font-size: 2rem;
        color: ${({ theme }) => theme.colors.white};
        font-weight: 700;

        @media (max-width: 750px){
            font-size: 1.7rem;
        }

        & span{
            font-size: 3rem;
            color: ${({ theme }) => darken(.5, theme.colors.white)};
            margin-right: 1rem;
            transform: translateY(1rem);

            @media (max-width: 750px){
                font-size: 2.3rem;
            }
        }
    }
`

export const RoomUsers = styled.div`
    width: 100%;
    padding-left: 5rem;

    & span{
        margin-top: 1rem;
        color: ${({ theme }) => theme.colors.white};
        font-size: 1.8rem;
        font-weight: 400;
        display: block;

        @media (max-width: 750px){
            font-size: 1.6rem;
        }
    }
`

export const OpenBar = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1000;
    width: 100%;
    display: none;
    height: 6rem;
    background-color: ${({ theme }) => theme.colors.grey};
    padding: 0 3rem;

    @media (max-width: 750px){
        display: flex;
        align-items: center;
        flex-direction: row-reverse;
    }
`

export const Burger = styled.div`
    width: 4rem;
    height: 3rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    transition: all .2s;
    cursor: pointer;
    transform:  ${(props: { open: boolean }) => props.open ? 'rotate(-15deg)' : 'rotate(0)'}; 
    

    & span{
        width: 100%;
        height: .4rem;
        background-color: ${({ theme }) => theme.colors.white};
        display: block;
        transition: all .2s;
        transition-delay: .1s;

        &:first-child{
            width: ${(props: { open: boolean }) => props.open ? '40%;' : '100%'} 
        }
        
        &:nth-child(2){
            width: ${(props: { open: boolean }) => props.open ? '70%;' : '100%'} 
        }

        &:last-child{
            width: ${(props: { open: boolean }) => props.open ? '90%;' : '100%'} 
        }
    }
`