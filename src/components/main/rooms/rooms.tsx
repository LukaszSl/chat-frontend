import React, { useContext, useEffect, useState } from 'react'
import { socket } from '../main'
import { Burger, ChatLogo, ChatName, OpenBar, Room, RoomsContainer, RoomsList, RoomUsers } from './rooms.style'
import logo from '../../../img/logo.svg'
import { Context } from '../../../context/context'

const rooms = [
    {
        name: 'Blue room',
        id: 'blue',
        users: ['user', "Lukasze", "Tukczyn"]
    },
    {
        name: 'Green room',
        id: 'green',
        users: []
    },
    {
        name: 'Red rum',
        id: 'red',
        users: []
    }
]


const Rooms: React.FC = () => {
    const [open, toggle] = useState(false)
    const context = useContext(Context)

    const joinRoom = (id: string) => {
        socket.emit('joinRoom', { room: id, username: context.username })
        context.setRoom(id)
    }

    return (
        <React.Fragment>
            <OpenBar>
                <Burger onClick={() => toggle(!open)} open={open}>
                    <span></span>
                    <span></span>
                    <span></span>
                </Burger>
            </OpenBar>
            <RoomsContainer open={open}>
                <ChatLogo src={logo} alt="" />
                <ChatName>
                    chat
                </ChatName>
                <RoomsList>
                    {rooms.map((r: any) => {
                        return (
                            <Room key={r.id} onClick={() => joinRoom(r.id)}>
                                <span>
                                    <span>#</span>
                                    {r.name}
                                </span>
                                <RoomUsers>
                                    {
                                        r.id === 'blue' && context.blueRoom.map((u: any) => {
                                            return <span key={u}>{u}</span>
                                        })
                                    }
                                    {
                                        r.id === 'red' && context.redRoom.map((u: any) => {
                                            return <span key={u}>{u}</span>
                                        })
                                    }
                                    {
                                        r.id === 'green' && context.greenRoom.map((u: any) => {
                                            return <span key={u}>{u}</span>
                                        })
                                    }
                                </RoomUsers>
                            </Room>
                        )
                    })}
                </RoomsList>
            </RoomsContainer>
        </React.Fragment>

    )
}

export default Rooms