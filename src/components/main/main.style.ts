import styled from 'styled-components'

export const MainContainer = styled.div`
    width: 100%;
    height: 100vh;
    background: ${({ theme }) => theme.colors.dark};
    display: flex;
`