import styled from 'styled-components'
import { darken } from 'polished'

export const MessagesContainer = styled.div`
    width: 100%;
    height: 90vh;
    overflow: scroll;
    overflow-x: hidden;
    display: flex;
    flex-direction: column;
    padding: 1rem;

    &::-webkit-scrollbar {
        width: 1rem;
    }
    &::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    }
    &::-webkit-scrollbar-thumb {
        background-color: ${({ theme }) => theme.colors.grey};
    }
`

export const MessageBox = styled.div`
    width: 45%;
    position: relative;
    min-height: 5rem;
    height: auto;
    padding: 1rem;
    background: ${(props: { own: boolean }) => props.own ? ({ theme }) => theme.colors.accent : ({ theme }) => darken(.2, theme.colors.white)};
    align-self: ${(props: { own: boolean }) => props.own ? 'flex-end' : 'flex-start'};
    margin: 1rem 0;
    border-radius: ${(props: { own: boolean }) => props.own ? '1rem 1rem 0 1rem' : '1rem 1rem 1rem 0'};
    color: ${(props: { own: boolean }) => props.own ? ({ theme }) => theme.colors.white : ({ theme }) => theme.colors.black};

    @media (max-width: 750px){
        font-size: 1.4rem;
        min-height: 4rem;
    }

    & span{
        position: absolute;
        top: -2rem;
        left: 1rem;
        font-size: 1.3rem;
        color: ${({ theme }) => darken(.2, theme.colors.white)};
    }
`