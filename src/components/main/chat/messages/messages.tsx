import React, { useContext } from 'react'
import { Context } from '../../../../context/context'
import { MessageBox, MessagesContainer } from './messages.style'

interface IProps {
    messages: any[]
}

const Messages: React.FC<IProps> = (props) => {
    const context = useContext(Context)

    return (
        <MessagesContainer>
            {
                props.messages.map((m: any, index: number) => {
                    return (
                        <MessageBox own={m.username === context.username} key={m.id}>
                            {m.text}
                            {((index > 0 && props.messages[index - 1].username !== m.username) || index === 0) && <span>{m.username}</span>}
                        </MessageBox>
                    )
                })
            }
        </MessagesContainer>
    )
}

export default Messages