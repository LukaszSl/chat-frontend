import styled from 'styled-components'

export const NewMessageContainer = styled.form`
    width: 100%;
    height: 5rem;
    background: ${({theme}) => theme.colors.grey};
    border-radius: 5rem;
    display: flex;
`

export const MessageInput = styled.input`
    width: calc(100% - 8rem);
    height: 100%;
    padding: 0 1.5rem;
    font-size: 1.8rem;
    border-radius: 5rem 0 0 5rem;
    background: ${({theme}) => theme.colors.grey};
    color: ${({theme}) => theme.colors.white};
`

export const SendButton = styled.button`
    width: 8rem;
    height: 100%;
    border-radius: 0 5rem 5rem 0;
    background: ${({theme}) => theme.colors.grey};
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;

    & img{
        height: 65%;
    }
`