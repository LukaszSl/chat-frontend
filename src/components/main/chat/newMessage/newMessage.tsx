import React, { FormEvent, useState } from 'react'
import { MessageInput, NewMessageContainer, SendButton } from './newMessage.style'
import arrow from '../../../../img/arrow.svg'

interface IProps {
    sendMessage: (mess: string) => void
}

const NewMessage: React.FC<IProps> = (props) => {
    const [message, setMessage] = useState('')
    const handleSubmit = (e: FormEvent) => {
        e.preventDefault()
        props.sendMessage(message)
        setMessage('')
    }

    return (
        <NewMessageContainer onSubmit={e => handleSubmit(e)}>
            <MessageInput type="text" placeholder="New message..." value={message} onChange={e => setMessage(e.target.value)} />
            <SendButton type="submit">
                <img src={arrow} alt=""/>
            </SendButton>
        </NewMessageContainer>
    )
}

export default NewMessage