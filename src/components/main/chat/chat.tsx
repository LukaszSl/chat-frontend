import React, { useContext, useEffect, useState } from 'react'
import { ChatContainer, SelectRoom } from './chat.style'
import NewMessage from './newMessage/newMessage'

import { socket } from '../main'
import Messages from './messages/messages'
import { Context } from '../../../context/context'


const Chat: React.FC = () => {
    const [response, setResponse] = useState('')
    const [messages, setMessages] = useState<{ text: string, username: string, id: string }[]>([])
    const context = useContext(Context)

    useEffect(() => {
        socket.on('newMessage', (data: any) => {
            setMessages([...messages, { text: data.text, username: data.username, id: data.id }])
        })

        socket.on('loadMessages', (data: any) => {
            setMessages(data.messages)
        })
    }, [messages])

    const sendMessage = async (message: string) => {
        try {
            if (!socket) {
                console.log("Houston jest problem")
                return;
            }
            // console.log(message)
            socket.emit('message', { text: message, room: context.room, username: context.username })
            setMessages([...messages, { text: message, username: context.username, id: Math.random().toString(36) }])
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <ChatContainer>
            {
                context.room !== '' ? (
                    <React.Fragment>
                        <Messages messages={messages} />
                        <NewMessage sendMessage={sendMessage} />
                    </React.Fragment>
                ) :
                    <SelectRoom>Select room</SelectRoom>
            }
        </ChatContainer>
    )
}

export default Chat