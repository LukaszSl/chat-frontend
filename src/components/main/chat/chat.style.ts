import styled from 'styled-components'

export const ChatContainer = styled.div`
    height: 100%;
    width: calc(100% - 55rem);
    padding: 2rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;

    @media (max-width: 1150px){
        width: calc(100% - 50rem);
    }

    @media (max-width: 960px){
        width: calc(100% - 30rem);
    }

    @media (max-width: 750px){
        width: 100%;
        padding-top: 6rem;
    }
`

export const SelectRoom = styled.h1`
    font-size: 4rem;
    color: ${({theme}) => theme.colors.white};
    margin-top: 4rem;
    font-weight: 900;
`