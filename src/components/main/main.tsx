import React, { useContext, useEffect, useState } from 'react'
import Chat from './chat/chat'
import { MainContainer } from './main.style'
import Rooms from './rooms/rooms'
import Users from './users/users'
import socketIOClient from 'socket.io-client'
import { Redirect } from 'react-router'
import { Context } from '../../context/context'
const ENDPOINT = 'http://localhost:1337'

export let socket = socketIOClient(ENDPOINT)

const Main: React.FC = (props) => {
    // const [room, setRoom] = useState('0')
    const context = useContext(Context)
    return (
        <MainContainer>
            {context.username === '' && <Redirect to="/login" />}
            <Rooms />
            <Chat />
            <Users />
        </MainContainer>
    )
}

export default Main