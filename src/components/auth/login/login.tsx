import React, { FormEvent, useContext, useState } from 'react'
import { AuthContainer, ErrorInfo, Form, FormTitle, Input, Submit, SwitchPage } from '../auth.style'
import axios from 'axios'
import { useHistory } from 'react-router'
import arrow from '../../../img/arrow.svg'
import { Context } from '../../../context/context'
import { Link } from 'react-router-dom'
import { socket } from '../../main/main'


const Login: React.FC = () => {
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassoword] = useState('')
    const history = useHistory()
    const context = useContext(Context)

    const handleSubmit = async (e: FormEvent) => {
        e.preventDefault()
        setLoading(true)
        try {
            const { data, status } = await axios.post('https://frozen-peak-95337.herokuapp.com/auth/local', {
                identifier: email,
                password
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })

            setLoading(false)
            if (status === 200) {
                context.setToken(data.jwt)
                context.setUsername(data.user.username)
                socket.emit('logged', { username: data.user.username })
                history.push('/')
            } else {
                setError('Invalid email/username or password')
            }
        } catch (error) {
            setError('Invalid email/username or password')
        }
    }

    return (
        <AuthContainer>
            <Form onSubmit={e => handleSubmit(e)}>
                <FormTitle>Sign in</FormTitle>
                <Input type="text" placeholder="email or username" onChange={(e) => setEmail(e.target.value)} value={email} />
                <Input type="password" placeholder="password" onChange={(e) => setPassoword(e.target.value)} value={password} />
                <SwitchPage>
                    Don't have an account?
                    <Link to='/register'>Register</Link>
                </SwitchPage>
                <Submit disabled={email === '' || password === '' || loading} type="submit">
                    <img src={arrow} alt="" />
                </Submit>
                {error !== '' && <ErrorInfo>{error}</ErrorInfo>}
            </Form>
        </AuthContainer>
    )
}

export default Login