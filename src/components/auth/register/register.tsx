import React, { FormEvent, useState } from 'react'
import { AuthContainer, ErrorInfo, Form, FormTitle, Input, Submit, SwitchPage } from '../auth.style'
import axios from 'axios'
import { Link, useHistory } from 'react-router-dom'
import arrow from '../../../img/arrow.svg'

const Register: React.FC = () => {
    const [loading, setLoading] = useState(false)
    const [email, setEmail] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassoword] = useState('')
    const [error, setError] = useState('')
    const history = useHistory()

    const handleSubmit = async (e: FormEvent) => {
        e.preventDefault()
        setLoading(true)
        try {
            const { data, status } = await axios.post('https://frozen-peak-95337.herokuapp.com/auth/local/register', {
                email,
                password,
                username
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })

            setLoading(false)
            if (status === 200) {
                history.push('/login')
            } else {
                setError('Invalid data')
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <AuthContainer>
            <Form onSubmit={e => handleSubmit(e)}>
                <FormTitle>Sign up</FormTitle>
                <Input type="text" placeholder="email" onChange={(e) => setEmail(e.target.value)} value={email} />
                <Input type="text" placeholder="username" onChange={(e) => setUsername(e.target.value)} value={username} />
                <Input type="password" placeholder="password" onChange={(e) => setPassoword(e.target.value)} value={password} />
                <SwitchPage>
                    Already have an account?
                    <Link to='/login'>Login</Link>
                </SwitchPage>
                <Submit disabled={email === '' || password === '' || username === '' || loading} type="submit">
                    <img src={arrow} alt="" />
                </Submit>
                {error !== '' && <ErrorInfo>{error}</ErrorInfo>}
            </Form>
        </AuthContainer>
    )
}

export default Register