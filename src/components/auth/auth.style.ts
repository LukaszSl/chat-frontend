import styled from 'styled-components'
import { darken, lighten } from 'polished'

export const AuthContainer = styled.div`
    width: 100%;
    height: 100vh;
    background-color: ${({ theme }) => theme.colors.dark};
    display: flex;
    justify-content: center;
    align-items: center;
`

export const Form = styled.form`
    width: 45rem;
    padding: 2rem;
    background-color: white;

    @media (max-width: 451px){
        padding: 5rem 2rem;
        width: calc(100% - 1rem);
    }
`

export const FormTitle = styled.h1`
    font-size: 3rem;
    font-weight: 300;
    text-transform: uppercase;
    text-align: center;
`

export const Input = styled.input`
    width: 100%;
    padding: 2rem;
    font-size: 2rem;
    font-weight: 300;
    background: ${({ theme }) => darken(.1, theme.colors.white)};
    margin-top: 2.5rem;
    transition: all .2s;
    border-radius: .2rem;

    &:focus{
        background: transparent;
        outline: 1px solid black;
    }

    @media (max-width: 451px){
        padding: 1.5rem;
        font-size: 1.7rem;
    }
`

export const Submit = styled.button`
    padding: 1rem 2rem 1rem 2.5rem;
    /* font-size: 10rem;
    line-height: 4rem;
    font-weight: 900; */
    display: flex;
    background: ${({ theme }) => theme.colors.accent};
    margin: auto;
    margin-top: 2.5rem;
    color: white;
    cursor: pointer;
    transition: all .1s;

    &:disabled{
        background-color: ${({ theme }) => darken(.1, theme.colors.white)}
    }

    & img{
        height: 7rem;
    }
`

export const SwitchPage = styled.p`
    font-size: 1.5rem;
    margin-top: 1rem;
    margin-left: 2rem;
    color: ${({ theme }) => lighten(.5, theme.colors.black)};
    & a{
        font-size: inherit;
        margin-left: .5rem;
        color: ${({ theme }) => theme.colors.accent};
    }
`

export const ErrorInfo = styled.p`
    color: #f33;
    text-align: center;
    margin-top: 1rem;
`