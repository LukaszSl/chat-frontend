import React, { useEffect, useState } from 'react';
import { Route, Switch } from 'react-router';
import Login from './components/auth/login/login';
import Register from './components/auth/register/register';
import Main, { socket } from './components/main/main';
import { Context } from './context/context';

function App() {
  const [token, setToken] = useState('')
  const [room, setRoom] = useState('')
  const [username, setUsername] = useState('')
  const [blueRoom, setBlueRoom] = useState<string[]>([])
  const [redRoom, setRedRoom] = useState<string[]>([])
  const [greenRoom, setGreenRoom] = useState<string[]>([])
  const [userList, setUserList] = useState<string[]>([])

  useEffect(() => {
    socket.emit('loadRooms')

    socket.on('rooms', (data: any) => {
      setBlueRoom(data.users.blue)
      setRedRoom(data.users.red)
      setGreenRoom(data.users.green)
      setUserList(data.userList)
      console.log("Im in rooms on", data)
    })

    socket.on('users', (data: any) => {
      console.log(data)
    })
  }, [])

  return (
    <Context.Provider value={{ username, setUsername, token, setToken, room, setRoom, blueRoom, redRoom, greenRoom, setBlueRoom, setRedRoom, setGreenRoom, userList }}>
      <Switch>
        <Route path='/' exact>
          <Main />
        </Route>
        <Route path='/login' exact>
          <Login />
        </Route>
        <Route path='/register' exact>
          <Register />
        </Route>
      </Switch>
    </Context.Provider>
  );
}

export default App;
