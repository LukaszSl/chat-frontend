import React from "react";

export const Context = React.createContext<{
    username: string,
    setUsername: (arg: string) => void,
    token: string,
    setToken: (arg: string) => void,
    room: string,
    setRoom: (arg: string) => void,
    blueRoom: string[],
    setBlueRoom: (arg: string[]) => void,
    redRoom: string[],
    setRedRoom: (arg: string[]) => void,
    greenRoom: string[],
    setGreenRoom: (arg: string[]) => void,
    userList: string[]
}>({
    username: '',
    setUsername: (arg: string) => { },
    token: '',
    setToken: (arg: string) => { },
    room: '',
    setRoom: (arg: string) => { },
    blueRoom: [],
    setBlueRoom: (arg: string[]) => { },
    redRoom: [],
    setRedRoom: (arg: string[]) => { },
    greenRoom: [],
    setGreenRoom: (arg: string[]) => { },
    userList: []
})
