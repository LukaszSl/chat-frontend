import { ThemeProvider } from 'styled-components'
import React from 'react'

const theme = {
    colors: {
        white: '#fff',
        black: '#000',
        grey: '#3A3F44',
        dark: '#23272A',
        darkGrey: '#2C2F33',
        accent: '#4D6687'
    },
    rwd: {
        large: '@media (max-width: 1440px)',
        medium: '@media (max-width: 1200px)',
        small: '@media (max-width: 880px)',
        mobile: '@media (max-width: 480px)'
    }
}

const Theme = ({ children }: any) => {
    return (
        <ThemeProvider theme={theme}>
            {children}
        </ThemeProvider>
    )
}

export default Theme